#!/usr/bin/env php
<?php

function sum_fibonacci($n)
{
	$sum = 0;

	$curr = 2;
	$last = 1;

	while ($curr < $n)
	{
		if ($curr % 2 == 0)
		{
			$sum += $curr;
		}

		$next = $last + $curr;
		$last = $curr;
		$curr = $next;
	}

	return $sum;
}

echo sum_fibonacci(4000000) . "\n";
