#!/usr/bin/env julia
#
#  04-palindrome.jl
#  euler
#

"""
Find the largest palindrome made from the product of two 3-digit numbers.
"""

function ispalindrome(x)
    s = string(x)
    for i = 1:div(length(s), 2)
        if s[i] != s[end + 1 - i]
            return false
        end
    end
    return true
end

h = 1
for i = 100:999
    for j = 100:999
        if ispalindrome(i * j)
            h = max(h, i * j)
        end
    end
end

println(h)
