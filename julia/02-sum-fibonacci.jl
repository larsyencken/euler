#!/usr/bin/env julia
#
#  02-sum-fibonacci.jl
#  euler
#
#  Sum all even elements of the fibonacci sequence whose values are less than
#  4 million.
#

function fib(lim)
    xs = Int[]
    push!(xs, 1)
    push!(xs, 2)
    i = 2
    x = xs[i] + xs[i - 1]
    while x < lim
        push!(xs, x)
        i += 1
        x = xs[i] + xs[i - 1]
    end
    sum(xs[mod(xs, 2) .== 0])
end

println(fib(4000000))
