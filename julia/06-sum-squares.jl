#!/usr/bin/env julia
#
#  06-sum-square-difference.jl
#  euler
#
#  Find the difference between the sum of squares and the square of the sum
#  of the first hundred natural numbers.
#

println(sum(1:100)^2 - sum([i^2 for i = 1:100]))
