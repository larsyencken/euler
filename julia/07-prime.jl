#!/usr/bin/env julia
#
#  07-prime.jl
#  euler
#
#  What is the 10001st prime number?
#

function primes(n)
    p = Int64[2, 3, 5, 7]
    next = 8
    while length(p) < n
        isprime = true
        for f in p
            if next % f == 0
                isprime = false
                break
            end
        end
        if isprime
            push!(p, next)
        end
        next += 1
    end

    p
end

println(primes(10001)[end])
