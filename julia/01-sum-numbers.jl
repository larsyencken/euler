#!/usr/bin/env julia
#
#  01-sum-numbers.jl
#  euler
#

s = 0
for i = 1:999
    if i % 3 == 0 || i % 5 == 0
        s += i
    end
end

println(s)
