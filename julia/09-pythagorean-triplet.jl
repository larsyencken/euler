#!/usr/bin/env julia
#
#  09-pythagorean-triplet.jl
#  euler
#
#  Find the pythagorean triplet a b c which sum to 1000.
#

for a in 1:997
    for b in a + 1:998
        c = 1000 - b - a
        if c > 0 && a^2 + b^2 == c^2
            println(a * b * c)
            exit()
        end
    end
end
