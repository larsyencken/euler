#!/usr/bin/env julia
#
#  05-divisible.jl
#  euler
#
#  2520 is the smallest number that can be divided by each of the numbers
#  from 1 to 10 without any remainder. What is the smallest positive number
#  that is evenly divisible by all of the numbers from 1 to 20?
#

function factorize(x)
    factors = Dict{Int64,Int64}()

    while x > 1
        for i = 2:x + 1
            if x % i == 0
                factors[i] = get(factors, i, 0) + 1
                x = div(x, i)
                break
            end
        end
    end

    factors
end

shared_factors = Dict{Int64,Int64}()
for i in 2:20
    factors = factorize(i)
    for (j, n) in collect(factors)
        shared_factors[j] = max(get(shared_factors, j, 0), n)
    end
end

println(reduce(*, [n^k for (n, k ) in collect(shared_factors)]))
