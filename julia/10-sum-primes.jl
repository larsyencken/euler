#!/usr/bin/env julia
#
#  10-sum-primes.jl
#  euler
#

xs = 2:1999999
println(sum(xs .* map(isprime, xs)))
