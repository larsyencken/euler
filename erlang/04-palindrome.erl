#!/usr/bin/env escript

main(_) ->
    Palindromes = [X * Y || X <- lists:seq(100, 999),
                            Y <- lists:seq(100, 999),
                            is_palindrome(X * Y)],
    MaxPalindrome = lists:max(Palindromes),
    io:format("~B\n", [MaxPalindrome]).

is_palindrome(X) when is_integer(X) ->
    [S] = io_lib:format("~B", [X]),
    is_palindrome(S);
is_palindrome(Xs) when is_list(Xs) ->
    lists:reverse(Xs) == Xs.
