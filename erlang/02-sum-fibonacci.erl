#!/usr/bin/env escript

main(_) ->
    S = sum_fibonacci(),
    io:format("~B\n", [S]).

sum_fibonacci() ->
    Fib = fib(4000000),
    EvenTerms = lists:filter(fun(X) -> X rem 2 == 0 end, Fib),
    lists:sum(EvenTerms).

fib(Max) ->
    fib_acc(0, 1, [1], Max).

fib_acc(XLast, X, Acc, Max) ->
    if
        X >= Max ->
           Acc
        ;
        true ->
            XNew = X + XLast,
            fib_acc(X, XNew, [XNew|Acc], Max)
    end.
