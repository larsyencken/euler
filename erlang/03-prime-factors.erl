#!/usr/bin/env escript
%
%  03-prime-factors.erl
%
%  Determine the largest prime factor of 600851475143.
%
%  The approach taken here uses Erlang's dynamic typing to implement lazy
%  sequences as lists.
%

main(_) ->
    Factors = eval_lazy(factors(600851475143)),
    io:format("~w\n", [Factors]).

lazy_count(K) ->
    [K|fun() -> lazy_count(K + 1) end].

% Return a lazy list, where the tail is evaluated by a function call
lazy_filter(Pred, Xs) ->
    case Xs of
        [] -> []
        ; [H|T] ->
            case Pred(H) of
                true ->
                    [H|fun() -> lazy_filter(Pred, T()) end];
                false ->
                    lazy_filter(Pred, T())
            end
    end.

eval_lazy(Xs) ->
    case Xs of
        [] -> [];
        [H|T] -> [H|eval_lazy(T())]
    end.

sieve([H|T]) ->
    [H|fun() -> sieve(lazy_filter(fun(X) -> X rem H /= 0 end, T())) end].

primes() -> sieve(lazy_count(2)).

factors(X) ->
    factors(X, primes()).

factors(X, [P|Ps]) ->
    MaxP = math:sqrt(X),
    if
        X == 1 -> [];
        P > MaxP -> [X|fun() -> [] end];
        true ->
            case X rem P == 0 of
                true ->
                    io:format("~B / ~B = ~B\n", [X, P, X div P]),
                    [P|fun() -> factors(X div P, Ps()) end];
                false ->
                    factors(X, Ps())
            end
    end.
