#!/usr/bin/env escript

main(_) ->
    S = sum_numbers(),
    io:format("~B\n", [S]).

sum_numbers() ->
    Range = lists:seq(1, 999),
    Multiples = lists:filter(fun(X) -> (X rem 3 == 0) or (X rem 5 == 0) end,
                             Range),
    lists:sum(Multiples).
