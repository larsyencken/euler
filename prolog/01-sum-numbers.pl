#!/usr/bin/env swipl -q -g main -s
%
%  01-sum-numbers.pl
%  euler
%
%  Created by Lars Yencken on 2012-02-29.
%  Coplright 2011 Lars Yencken All rights reserved.
%

main :-
    current_prolog_flag(argv, Argv),
    append(_, [--|_Av], Argv), !,
    main(Argv).

main(_Argv) :-
    sum_numbers(1000, Sum),
    print(Sum),
    print('\n'),
    halt.

sum_numbers(N, Sum) :- sum_numbers(N, 0, Sum).

sum_numbers(N0, Acc0, Acc) :-
    (N0 > 1 ->
        N is N0 - 1,
        ((0 is mod(N0, 5) ; 0 is mod(N0, 3)) ->
            Acc1 is Acc0 + N0,
            sum_numbers(N, Acc1, Acc)
        ;
            sum_numbers(N, Acc0, Acc)
        )
    ;
        Acc = Acc0
    ).
