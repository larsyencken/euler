#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  02-sum-fibonacci.py
#  euler
#
#  Created by Lars Yencken on 2012-02-18.
#  Copyright 2012 Lars Yencken. All rights reserved.
#

from itertools import takewhile

def fibonacci():
    yield 1
    yield 2
    a = 1
    b = 2

    while True:
        yield a + b
        a, b = b, a + b

print sum([x for x in takewhile(lambda x: x < 4000000, fibonacci())
    if x % 2 == 0])
