#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  10-sum-primes.py
#  euler
#
#  Created by Lars Yencken on 2012-02-18.
#  Copyright 2012 Lars Yencken. All rights reserved.
#

from itertools import takewhile

def primes():
    "Sieve of Eratosthenes"
    d = {}
    q = 2
    while True:
        p = d.pop(q, None)
        if p:
            x = p + q
            while x in d:
                x += p
            d[x] = p
        else:
            d[q*q] = q
            yield q
        q += 1

print sum(takewhile(lambda x: x < 2000000, primes()))

