#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  05-divisble.py
#  Code
#
#  Created by Lars Yencken on 2012-02-11.
#  Copyright 2012 99designs. All rights reserved.
#

"""
2520 is the smallest number that can be divided by each of the numbers from
1 to 10 without any remainder. What is the smallest positive number that is
evenly divisible by all of the numbers from 1 to 20?
"""

import operator

def factorize(x):
    factors = []

    while x > 1:
        for i in xrange(2, x + 1):
            if x % i == 0:
                factors.append(i)
                x /= i
                break

    return factors

def max_factors(xs):
    dist = {}
    for x in xs:
        fs = factorize(x)
        for f in set(fs):
            dist[f] = max(dist.get(f, 0), fs.count(f))

    return reduce(operator.mul, (f**p for (f, p) in dist.iteritems()))

print max_factors(range(20))
