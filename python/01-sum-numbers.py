#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  01-sum-numbers.py
#  euler
#
#  Created by Lars Yencken on 2012-02-18.
#  Copyright 2012 Lars Yencken. All rights reserved.
#

print sum([x for x in xrange(1, 1000) if x % 3 == 0 or x % 5 == 0])
