#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  09-pythagorean-triplet.py
#  euler
#
#  Created by Lars Yencken on 2012-02-18.
#  Copyright 2012 Lars Yencken. All rights reserved.
#

"""
There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""

from math import sqrt

def get_triplet():
    for a in xrange(1, 500):
        for b in xrange(a+1, 500):
            c = 1000 - a - b
            if c*c == a*a + b*b:
                return a, b, c

a, b, c = get_triplet()
print a * b * c
