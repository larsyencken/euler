#!/usr/bin/env clj
;
;  10-sum-primes.py
;  euler
;
;  Created by Lars Yencken on 2012-02-18.
;  Copyright 2012 Lars Yencken All rights reserved.
;

; Find the sum of all the primes below two million.

(ns euler
  (:use clojure.contrib.lazy-seqs))

(println (apply + (take-while #(< % 2000000) primes)))
