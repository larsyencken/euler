#!/usr/bin/env clj
;
;  07-numbered-prime.py
;  code
;
;  Created by Lars Yencken on 2012-02-17.
;  Copyright 2012 Lars Yencken All rights reserved.
;

(ns euler
  (:use clojure.contrib.lazy-seqs))

(println
  "Answer:"
  (nth primes 10000))
