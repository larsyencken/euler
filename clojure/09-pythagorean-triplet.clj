#!/usr/bin/env clj
;
;  09-pythagorean-triplet.py
;  euler
;
;  Created by Lars Yencken on 2012-03-02.
;  Copyright 2012 Lars Yencken. All rights reserved.
;

; Find the Pythagorean triplet for which a + b + c = 1000.

(ns euler
  (:use clojure.contrib.combinatorics))

(defn is-triplet? [a b]
  (let [c (- 1000 a b)]
    (= (* c c) (+ (* a a) (* b b)))))

(defn get-triplet []
  (let [xs (range 1 500)
        pairs (combinations xs 2)]
    (first (filter #(apply is-triplet? %) pairs))))

(let [[a b] (get-triplet)
      c (- 1000 a b)
      prod (* a b c )]
  (println "Answer: " prod))

