(define sum-fib
  (lambda (f max-val)
    (sum-fib-acc f 1 2 max-val 2)))

(define sum-fib-acc
  (lambda (f last2 last1 max-val acc)
    (let ((next (+ last2 last1)))
      (if (> next max-val)
        acc
        (sum-fib-acc f last1 next max-val (if (f next) (+ acc next) acc))))))

(display
  (sum-fib
    (lambda (x) (= 0 (modulo x 2)))
    4000000))
