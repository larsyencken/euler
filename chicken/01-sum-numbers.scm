(use srfi-1)

(define sum-numbers
  (lambda (factors total)
    (sum-acc factors (apply min factors) total 0)))

(define sum-acc
  (lambda (factors x last acc)
    (if (= x last)
      acc
      (let ((new-acc (+ acc (if (is-factor x factors) x 0))))
        (sum-acc factors (+ x 1) last new-acc)))))

(define is-factor
  (lambda (x factors)
    (any (lambda (f) (= (modulo x f) 0)) factors)))

(display
  (sum-numbers '(3 5) 1000))
