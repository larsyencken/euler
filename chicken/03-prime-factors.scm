(define prime-factors
  (lambda (p)
    (prime-factors-acc 2 p '())))

(define prime-factors-acc
  (lambda (f p acc)
    (let* ((x (next-factor f p))
           (p-next (quotient p x))
           (acc-next (cons x acc)))
      (assert (> p-next 0))
      (if (= p-next 1)
        acc-next
        (prime-factors-acc x p-next acc-next)))))

(define next-factor
  (lambda (f p)
    (assert (<= f p))
    (if (= 0 (modulo p f))
      f
      (if (> f (sqrt p))
        p
        (next-factor (+ f 1) p)))))

(display
  (apply max (prime-factors 600851475143)))
