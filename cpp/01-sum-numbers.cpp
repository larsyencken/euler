/*
 *
 *  01-sum-numbers.c
 *  euler
 *
 *  Created by Lars Yencken on 2012-02-29.
 *  Copyright 2012 Lars Yencken. All rights reserved.
 *
 */

#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
	int sum = 0;

	for (int i = 1; i < 1000; ++i)
	{
		if (i % 3 == 0 || i % 5 == 0)
		{
			sum += i;
		}
	}
	cout << sum << endl;
	return 0;
}
