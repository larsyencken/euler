/*
 *
 *  03-prime-factors.c
 *  euler
 *
 *  Created by Lars Yencken on 2012-03-01.
 *  Copyright 2012 Lars Yencken. All rights reserved.
 *
 */

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

/**
 * Find all the primes between 2 and n.
 */
void prime_sieve(uint64_t n, vector<uint64_t>& primes)
{
	primes.clear();

	vector<bool> is_prime;
	is_prime.reserve(n + 2);

	// initialise to true
	for (uint64_t i = 0; i <= n; i++)
	{
		is_prime.push_back(true);
	}

	for (uint64_t i = 2; i <= n; i++)
	{
		if (is_prime[i])
		{
			// mark all its multiples as not prime
			for (uint64_t j = i + i; j <= n; j += i)
			{
				is_prime[j] = false;
			}
		}
	}

	// keep the primes we found
	for (uint64_t i = 2; i <= n; i++)
	{
		if (is_prime[i])
		{
			primes.push_back(i);
		}
	}
}

/**
 * Factorize n using the trial division method, storing each prime factor
 * into the given factors vector.
 */
void trial_division(uint64_t n, vector<uint64_t>& factors)
{
	factors.clear();

	vector<uint64_t> primes;
	prime_sieve((uint64_t)pow(n, 0.5) + 1, primes);
	uint64_t p;
	for (vector<uint64_t>::iterator it = primes.begin(); it != primes.end();
			it++)
	{
		p = *it;
		if (p * p > n)
		{
			break;
		}
		while (n % p == 0)
		{
			factors.push_back(p);
			n /= p;
		}
	}

	if (n > 1)
	{
		factors.push_back(n);
	}
}

int main(int argc, const char *argv[])
{
	const uint64_t n = 600851475143;
	vector<uint64_t> factors;
	trial_division(n, factors);
	cout << factors[factors.size() - 1] << endl;
	return 0;
}
