/*
 *
 *  02-sum-fibonacci.c
 *  euler
 *
 *  Created by Lars Yencken on 2012-03-01.
 *  Copyright 2012 Lars Yencken. All rights reserved.
 *
 */

#include <iostream>

using namespace std;

int sum_even_fibonacci(int n)
{
	int sum = 0;
	int last = 1;
	int curr = 2;
	int tmp;

	while (curr < n)
	{
		if (curr % 2 == 0)
		{
			sum += curr;
		}

		tmp = last;
		last = curr;
		curr += tmp;
	}

	return sum;
}

int main(int argc, const char *argv[])
{
	cout << sum_even_fibonacci(4000000) << endl;
	return 0;
}
