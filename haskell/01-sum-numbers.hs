--
-- 01-sum-numbers.hs
--
-- Sum all the numbers in the range [1,1000). 
--

sumNumbers = sum [i | i <- [1..999], i `mod` 3 == 0 || i `mod` 5 == 0]

main = do putStrLn (show sumNumbers)
