#!/usr/bin/env scala
!#
//
//  02-sum-fibonnaci.py
//  euler
//
//  Created by Lars Yencken on 2012-02-18.
//  Copyright 2012 Lars Yencken. All rights reserved.
//

import scala.annotation.tailrec

def fibonacci: Stream[Int] = {
  Stream.cons(1, Stream.cons(2, fibonacciNext(1, 2)))
}

def fibonacciNext(a: Int, b: Int): Stream[Int] = {
  Stream.cons(a + b, fibonacciNext(b, a + b))
}

println(fibonacci.takeWhile(_ < 4000000).filter(_ % 2 == 0).foldLeft(0)(_ + _))
