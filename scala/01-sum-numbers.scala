#!/usr/bin/env scala
!#
//
//  01-sum-numbers.py
//  euler
//
//  Created by Lars Yencken on 2012-02-18.
//  Copyright 2012 Lars Yencken. All rights reserved.
//

def ismultiple(x: Int) = {
  x % 5 == 0 || x % 3 == 0
}

println(1.to(999).filter(ismultiple).foldLeft(0)(_ + _))
